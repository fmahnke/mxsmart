import urllib
import urllib2
import time

def build_url(baseurl, get_params):
    params = ''
    for k, v in get_params.iteritems():
        if params != '':
            params += '&'
        params = params + k + '=' + v

    return baseurl + params

def make_request(req):
    return urllib2.urlopen(req)

def get(url):
    req = urllib2.Request(url)
    response = make_request(req)
    return response.read()

def post(url, values):
    data = urllib.urlencode(values)
    req = urllib2.Request(url, data)
    response = make_request(req)
    return response.read()

