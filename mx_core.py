import os
import logging
import tempfile

import mx_object as mxo
import util.fileutil as futil
import mx_cache as cache
import mx_to_cgats
import request

class Config:
    CONFIG_FILE_PATH = 'mx_config.json'

    def __init__(self):
        self._config = futil.from_file(Config.CONFIG_FILE_PATH)

    def get(self, property):
        if property in self._config:
            return self._config[property]
        else:
            return None

'''
General
'''

class Maxwell:
    def _init_logger(self, level, filename):
        if 'debug' == level.lower():
            log_level = logging.DEBUG
        elif 'warning' == level.lower():
            log_level = logging.WARNING
        elif 'error' == level.lower():
            log_level = logging.ERROR
        else:
            log_level = logging.INFO

        self.log = logging.getLogger('maxwell')
        self.log.setLevel(log_level)
        ch = logging.StreamHandler()
        ch.setLevel(log_level)

        formatter = logging.Formatter('%(asctime)s %(levelname)s | %(funcName)s | %(message)s',
            datefmt='%m/%d/%Y %H:%M:%S')
        ch.setFormatter(formatter)
        self.log.addHandler(ch)

        if filename:
            fh = logging.FileHandler(filename)
            fh.setLevel(log_level)
            fh.setFormatter(formatter)
            self.log.addHandler(fh)

    def __init__(self):
        self.config = Config()

        self._init_logger(self.config.get('loglevel'), self.config.get('logfile'))

        self.req = request.Request(self.config.get('http_retry_delay'), self.log)
        self.cache_path = self.config.get('cscl_cache_path')

        user_mxid = self.config.get('user_mxid')

        self.log.info('Updating device list for user with MxID %s' % user_mxid)

        self.udl = self.req.user_device_list(user_mxid)

    def _get_track_color_set_list(self, device_mxid, track_name=None):
        ''' Get color set list for all tracks for the specified device. '''

        track_list = self.req.device_track_list(device_mxid[0])

        track_mxids = mxo.find_mxid(track_list, track_name)

        self.log.debug(track_mxids)
        cscl = []
        for mxid in track_mxids:
            track_cscl = self.req.track_color_set_list(mxid)
            cscl.append(track_cscl)
        return cscl

    def _fetch_color_set_compound_objects(self, mxids):
        '''
        Given a list of mxids for ColorSet objects, fetch them from Maxwell using its API.
        '''

        fetched_from_server = self.req.color_set_compound(mxids)

        assert len(mxids) == len(fetched_from_server)

        validated_from_server = mxo.valid_color_sets(fetched_from_server)

        return validated_from_server

    def _get_color_set_compound_objects(self, mxids):
        '''
        Get any number of ColorSet objects using their mxids from the cache. Return fetched ColorSet
        objects and a list of objects that were not found in the cache.
        '''
        data = []
        need_from_server = []

        self.log.info('Getting ' + str(len(mxids)) + ' CS')
        self.log.debug('MxIDs: %s' % mxids)

        self.log.info('Loading color sets from cache.')
        cs = cache.load_mx_objects(self.cache_path, mxids)

        if cs is None:
            need_from_server = mxids
        else:
            loaded_from_cache = []

            for id in mxids:
                if id not in cs.keys():
                    need_from_server.append(id)
                else:
                    loaded_from_cache.append(id)
                    data.append(cs[id])

            self.log.info(str(len(loaded_from_cache)) + ' CS were loaded from cache.')
            self.log.debug('MxIDs: %s' % loaded_from_cache)
        
        self.log.info(str(len(need_from_server)) + ' CS were not found in cache.')
        self.log.debug('MxIDs: %s' % need_from_server)

        # Return CSs loaded from cache and a list of what is needed from server.
        return data, need_from_server

    ''' Web API functions '''

    def get_colorset_compound(self, device_name, track_name=None, user_name=None):
        if user_name is not None:
            # Get a device that belongs to a different user.

            top_user_mxid = self.config.get('user_mxid')
            top_user_contact_list = self.req.user_contact_list(top_user_mxid)

            self.log.debug('User contact list for user with mxid {0}: {1}'
                    .format(top_user_mxid, top_user_contact_list))

            user_mxid = mxo.find_mxid(top_user_contact_list, user_name)[0]

            self.log.debug('User mxid is {0}'.format(user_mxid))

            if user_mxid is None:
                self.log.error('User with mxid {0} does not have a contact with name {1}'
                        .format(top_user_mxid, user_name))

                return None, None

            user_device_list = self.req.user_device_list(user_mxid)

            self.log.debug('User device list for {0}: {1}'.format(user_name, user_device_list))

            device_mxid = mxo.find_mxid(user_device_list, device_name)
        else:
            device_mxid = mxo.find_mxid(self.udl, device_name)

        if device_mxid is None:
            raise ValueError('Device with name {0} does not exist'.format(device_name))

        """
        TODO: Presently, we assume the device name is unambiguous. An assertion
        error will happen if someone searches for a partial device name here.
        """
        assert len(device_mxid) == 1

        tcsl = self._get_track_color_set_list(device_mxid, track_name = track_name)

        tcsl_mxids = []
        for one_tcsl in tcsl:
            one_mxid_set = mxo.get_mxids(one_tcsl)
            for mxid in one_mxid_set:
                tcsl_mxids.append(mxid)
        cscos, still_needed = self._get_color_set_compound_objects(tcsl_mxids)
        self.log.debug('number of cscos: ' + str(len(cscos)))
        return cscos, still_needed

    def update_cache(self):
        ''' Update the Mx measurement cache for all devices specified in the configuration file.'''
        self.log.info('Beginning requested cache update.')

        cache_lock = cache.get_lock()

        if cache_lock is None:
            self.log.info('Couldn\'t get a lock on the cache. Aborting cache update.')
            return False

        user = self.config.get('user')
        devices = self.config.get('devices')

        for device in devices: 
            self.log.info('Updating track color set list cache for ' + device)
            cscos, need_from_server = self.get_colorset_compound(device, track_name=None,
                    user_name=user if user else None)

            if need_from_server:
                self.log.info('Fetching %s color sets from Maxwell.' % len(need_from_server))
                fetched_from_server = self._fetch_color_set_compound_objects(need_from_server)

                valid_count = len(fetched_from_server)
                incomplete_count = len(need_from_server) - len(fetched_from_server)

                self.log.info('Fetched %s valid, %s incomplete color sets.' % (valid_count,
                    incomplete_count))
                self.log.info('Saving %s fetched color sets to cache.' % len(fetched_from_server))
                cache.save_mx_objects(self.cache_path, fetched_from_server)

        self.log.info('Finished requested cache update.')

        cache.unlock(cache_lock)

        return True

    def get_api_passfail(self, device, start_time=None, end_time=None):
        ''' Return pass/fail list for the specified device and tracks, optionally filtering by date range. '''
        cscos, need_from_server = self.get_colorset_compound(device)
        cscos = mxo.filter_cscos_by_date(cscos, start_time, end_time)
        self.log.debug('filtered ' + str(len(cscos)))
        date_and_status = {}
        for csco in cscos:
            date_and_status[mxo.creationdate(csco)] = mxo.passfail(csco)
        return date_and_status

    def get_api_csco(self, device, start_time=None, end_time=None):
        ''' Get all colorset color lists for the specified device and tracks, optionally filtering by date range. '''
        pass

    def get_colorsets(self, device, track_name=None, start_time=None, end_time=None):
        ''' Get all colorsets for the specified device and tracks, optionally filtering by date range. '''
        cscos, not_loaded = self.get_colorset_compound(device, track_name=track_name)

        if (start_time is not None and end_time is not None):
            cscos = mxo.filter_cscos_by_date(cscos, start_time, end_time)

            self.log.info('Filtered %s between %s and %s' % (len(cscos), start_time, end_time))

        to_zip = []

        temp_dir = tempfile.gettempdir()

        for cscl in cscos:
            self.log.debug('Saving CGATS file for mxid %s' % cscl['MXobject']['MxID'])

            filename = cscl['MXobject']['fields']['CS_colorSetName']['fieldValue']
            mxid = mxo.get_mxids(cscl)
            assert len(mxid) == 1
            cscl_data = cscl['MXobject']['relatedMXobjects']
            assert len(cscl_data.keys()) == 1
            creation_date = mxo.creationdate(cscl)

            try:
                cgats = mx_to_cgats.cscl_to_cgats(creation_date, cscl_data['CSCL:' + mxid[0]])
            except TypeError:
                self.log.error('Unable to create CGATS file for mxid %s' % cscl['MXobject']['MxID'])
                continue

            with open(temp_dir + '/' + filename, 'w') as f:
                f.write(cgats)
            to_zip.append(temp_dir + '/' + filename)

        if len(to_zip):
            futil.zip_files(temp_dir + '/response.zip', to_zip)

            return temp_dir + '/response.zip'
        else:
            return None
