## Getting started

Run the server using its script `./server`.

## HTTP GET endpoints

- get_colorsets - Request measurements. Only colorsets with a complete measurement and complete
pass/fail status will be returned.
- get_passfail - Request pass/fail data

### Request query string examples

GET requests are made by sending a JSON string to the endpoint.

To request all measurements from one device, filtered by date, send the following URL-encoded JSON
string to the get_colorsets endpoint:

{
	"device": "phx-igen-313",
	"filter": {
		"startDateTime": "2013-08-28 00:00:00 UTC",
		"endDateTime": "2013-08-29 00:00:00 UTC"
	}
}

Encoded for URL: %7B%22device%22%3A%22phx-igen-313%22%2C%22filter%22%3A%7B%22startDateTime%22%3A%222013-08-28%2000%3A00%3A00%20UTC%22%2C%22endDateTime%22%3A%222013-08-29%2000%3A00%3A00%20UTC%22%7D%7D

Send the same URL-encoded string to the get_passfail endpoint to retrieve only pass/fail statistics.

## Tests

Tests are run by `python test_mx_all.py` and `python test_request.py`

## To do

* Consider making field lookups case insensitive
* remove temp_cache directory after completing tests
* Consider supporting requests for different time zones.

## Bugs

* Log file isn't created if the log directory doesn't exist.
* Better logging filters.

## Code conventions

These abbreviations are introduced in the Maxwell API documentation and are used by convention in
the code:

mxo - Maxwell Object  
u - user object  
d - device object  
dtl - device track list  
utl - user track list  
t - track  
tcsl - track color set list (measurement)  
cs - color set  
cscl - color set's color list  
c - color  

