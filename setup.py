from setuptools import setup

setup(
    name='mxsmart',
    version='0.0.0',
    author='Fritz Mahnke',
    author_email='fritz@fritzmahnke.com',
    description='A web service to fetch data from the CHROMiX Maxwell API.',
    keywords='color_management color',
    packages=['mxsmart']
)
