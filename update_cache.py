#!/usr/bin/env python

import mx_core as mx_c

import sys

mx = mx_c.Maxwell()

def update():
    try:
      while 1:
        print "Requesting cache update"
        result = mx.update_cache()

        if result == True:
          print "Cache update successful"
        else:
          print "Cache update failed. Aborting"
          break
    except KeyboardInterrupt:
      print "finished"
    except:
      print "unknown error. Retrying", sys.exc_info()[0]
      update()

update()
