import time

def current_time():
    ''' Return a string representing the current UTC time. '''
    return time.strftime('%Y-%m-%d %H:%M:%S UTC', time.gmtime())

def _find_key_recursive(d, key):
    if key in d:
        return d[key]
    for k, v in d.iteritems():
        if type(v) is dict:
            value = _find_key_recursive(v, key)
            if value:
                return value

def _find_fields(mx_data):
    return _find_key_recursive(mx_data, 'fields')

def field_mappings(mx_data):
    fields = _find_fields(mx_data)
    mappings = {}
    for k in fields:
        fieldUID = fields[k]['fieldUID']
        mappings[fieldUID] = k
    return mappings

def get_field_value(mx_data, row, field_name):
    mappings = field_mappings(mx_data)
    return mx_data['rows'][row][mappings[field_name]]['fieldValue'] 

def get_mxids(mxo):
    ''' Return a list of all the MxIDs in the given MXobject. '''
    mxo_ids = []
    try:
        # Standard MxO list
        mappings = field_mappings(mxo)
        for d in mxo['MXobjects']['rows']:
            mxo_ids.append(d[mappings['MxID']]['fieldValue'].split(':')[-1])
    except KeyError:
        # Compound object
        mxo_ids.append(mxo['MXobject']['MxID'].split(':')[-1])

    return mxo_ids

def get_rows(mx_data):
    ''' Get the rows from an MxO and return them as a list of dictionaries. '''
    mappings = field_mappings(mx_data)
    rows = []
    fields = []
    for field in mappings:
        fields.append(field)
    for row in mx_data['rows']:
        one_row = {}
        for field in mappings:
            # Build dict 
            one_row[field] = row[mappings[field]]['fieldValue']
        rows.append(one_row)
    return fields, rows

def get_cscl(mx_object, mxid):
    ''' Find and return the CSCL in an MxO. '''
    assert len(mx_object['MXobject']['relatedMXobjects']) == 1
    return mx_object['MXobject']['relatedMXobjects']['CSCL:' + mxid]

def valid_color_sets(mx_objects):
    ''' 
    Given a list of MxOs, return a list containing the ones that are valid (complete) color
    sets.
    '''
    valid_color_sets = []

    for obj in mx_objects:
        color_set_type = _find_key_recursive(obj, 'CS_colorSetType')['fieldValue']
        pass_fail_status = _find_key_recursive(obj, 'CS_notifierMark')['fieldValue']
        if "-1" != color_set_type:
            valid_color_sets.append(obj) 

    return valid_color_sets

def find_mxid(mx_object_list, name):
    ''' Given an mxobject list, find and return a list of mxids corresponding to the given name.
    
    Returns None if there are no mxids corresponding to the given name.

    If no name is provided, return all mxids.
    '''
    if name is None:
        name = ''

    matches = []
    mappings = field_mappings(mx_object_list)
    for obj in mx_object_list['MXobjects']['rows']:
        if name.lower() in obj[mappings['MXobjectName']]['fieldValue'].lower() :
            matches.append(obj[mappings['MxID']]['fieldValue'].split(':')[-1])

    if len(matches) == 0:
        return None

    return matches

def struct_time(mx_date):
    ''' Transform an mx date string into a Python struct_time object. '''
    return time.strptime(mx_date, '%Y-%m-%d %H:%M:%S %Z')

def is_between_times(op_time, start_time, end_time):
    ''' Determine if a time is within a time range inclusive. '''
    assert op_time is not None

    return start_time <= op_time <= end_time

def creationdate(mx_object):
    ''' Get the creation date from an mx object. '''
    ''' TODO: This is the date the object is created on the Mx server. It is
    not equivalent to the measurement date, which we need to address. '''
    return _find_key_recursive(mx_object, 'MXcreationDate')

def passfail(mx_colorset):
    ''' Get the pass/fail result from a colorset. '''
    return _find_key_recursive(mx_colorset, 'CS_notifierMark')['fieldValue']

def filter_cscos_by_date(mx_objects, start_time, end_time):
    ''' Filter a sequence of mx objects by creation date. '''

    assert start_time is not None and end_time is not None

    return filter(lambda x: is_between_times(struct_time(creationdate(x)), start_time, end_time), mx_objects)

