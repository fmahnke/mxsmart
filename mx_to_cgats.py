import mx_object as mxo
import cgats_writer

mx_to_cgats_field_mappings = {'C_sampleID': 'SampleID', 'C_sampleName': 'SAMPLE_NAME'}
 
def _mx_fields_to_data_fields(fields, rows):
    data_format = ''
    if 'C_sampleID' in fields:
        data_format += 'SampleID\t'
    else:
        return None
    if 'C_sampleName' in fields:
        data_format += 'SAMPLE_NAME\t'
    else:
        return None

    if 'RGB' in rows[0]['C_deviceSpace']:
        data_format += 'RGB_R\tRGB_G\tRGB_B\t'
    elif 'CMYK' in rows[0]['C_deviceSpace']:
        data_format += 'CMYK_C\tCMYK_M\tCMYK_Y\tCMYK_K\t'
    else:
        return None

    if 'C_CIELabValues' in fields:
        data_format += 'LAB_L\tLAB_A\tLAB_B'
    else:
        return None

    return data_format

def _mx_rows_to_data_section(rows):
    rows_string = ''
    for row in rows:
        one_row_string = ''
        one_row_string += row['C_sampleID'] + '\t'
        one_row_string += row['C_sampleName'] + '\t'
        if 'RGB' in row['C_deviceSpace']:
            rgb = row['C_deviceValues'].split()
            one_row_string += rgb[0] + '\t'
            one_row_string += rgb[1] + '\t'
            one_row_string += rgb[2] + '\t'
        elif 'CMYK' in row['C_deviceSpace']:
            cmyk = row['C_deviceValues'].split()
            one_row_string += cmyk[0] + '\t'
            one_row_string += cmyk[1] + '\t'
            one_row_string += cmyk[2] + '\t'
            one_row_string += cmyk[3] + '\t'
        else:
            return None
        lab = row['C_CIELabValues'].split()
        one_row_string += lab[0] + '\t'
        one_row_string += lab[1] + '\t'
        one_row_string += lab[2]
        rows_string += one_row_string + '\n'

    return rows_string

def cscl_to_cgats(creation_date, cscl_data):
    ''' Convert an Mx CSCL to tab-delimited CGATS data. '''

    # Construct data from cscl
    fields, rows = mxo.get_rows(cscl_data)

    data_section = _mx_rows_to_data_section(rows)

    cgats_data = cgats_writer.data(data_section)
    cgats_data_format = cgats_writer.data_format(_mx_fields_to_data_fields(fields, rows))
    cgats_header = cgats_writer.header(creation_date)

    return cgats_header + cgats_data_format + cgats_data

