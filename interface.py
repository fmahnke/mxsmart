import mx_core as mx_c
import mx
import pprint
import pickle
import json

if __name__ == '__main__':

    pp = pprint.PrettyPrinter()
    c = mx_c.Config()
    r = mx_c.Request(c.config['http_retry_delay'])
    # dev_list = mx_c.get_device_list_json(r, '30238')
    # user_track_list = mx_c.get_track_list_json(r, '30351')
    dev_track_list = mx_c.get_device_track_list_json(r, ['3031'])
    user_track_list = mx_c.json_from_file('tests/mx_sample_user_track_list.json')
    # tscl = pickle.load(open('mx_c_sample_tcsl.pickle', 'rb'))

    ''' Testing fetch operations '''
    cs_mxids = ['232301', '232245', '231407']
    cs = mx_c.get_color_set_compound(r, cs_mxids)

    ''' Load test JSON '''
    json_cs_compound = json.loads(open('tests/mx_sample_cs_compound.json').read())

    ''' Load all measurements from a track '''

    # Get the track measurement list
    tcsl = mx_c.get_track_color_set_list(r, ['3347'])
    # Get a Python list of mxids for measurements
    cs_list = mx_c.get_mxids([tcsl['3347']])
    # Get all ColorSet objects
    all_colorsets = mx.get_color_sets(r, cs_list[0])

    # data = json.loads(cs_compound['229049'])
    # cscl = mx_c.get_cscl(cs_compound['229049'], '229049')
    # data['MXobject']['relatedMXobjects']['CSCL:229049']

    # id = c.get_device_mxid('PHX-iGen-313')
    # mxids = get_mxids(track_list)

    # Proposed API examples
    # tcsl = mx_c.get_color_set_list(track_number)
    # measurements = mx_c.colorset.get(tcsl) # Fetch lots of measurements from cache or server
    
