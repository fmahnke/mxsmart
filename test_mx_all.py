import json
import os
import shutil
import unittest

import util.fileutil as futil
import mx_core as mx_c
import mx_object as mxo
import mx_cache as cache
import mx_to_cgats

TEMP_CACHE_PATH = 'temp_cache'
TEST_CACHE_PATH = 'tests/cache'
NONEXISTENT_CACHE_PATH = 'tests/bogus_cache_path'

class TestMxObject(unittest.TestCase):
    def test_find_mxid(self):
        ''' Find a device's mxid with its name. '''
        dev_list = futil.from_file('tests/mx_sample_dev_list.json')
        self.assertEquals(['3194'], mxo.find_mxid(dev_list, 'phx-igen-302'))
        self.assertEquals(['3194'], mxo.find_mxid(dev_list, 'PHx-iGen-302'))
        self.assertEquals(['3153'], mxo.find_mxid(dev_list, 'ftm-indigo-232'))
        self.assertEquals(['3153'], mxo.find_mxid(dev_list, 'FtM-indigo-232'))
        
    def test_find_mxid_partial(self):
        ''' Find a track's mxids matching a partial name. '''
        track_list = futil.from_file('tests/mx_sample_dev_track_list.json')
        self.assertItemsEqual(['3343', '3341', '3339'], mxo.find_mxid(track_list, 'phx-303'))
        self.assertItemsEqual(['3341', '3339'], mxo.find_mxid(track_list, 'phx-303 mohawk'))

    def test_find_mxid_nonexistent(self):
        ''' Find a nonexistent device's mxid with its name. '''
        dev_list = futil.from_file('tests/mx_sample_dev_list.json')
        self.assertEquals([], mxo.find_mxid(dev_list, 'no_real_device'))

    def test_get_mxids_compound(self):
        cscos = futil.from_file('tests/mx_sample_cs_compound.json')
        ids = [mxo.get_mxids(mx_colorset_compound) for mx_colorset_compound in cscos]
        self.assertEquals(3, len(ids))

    def test_get_mxids(self):
        mx_objects = futil.from_file('tests/mx_sample_user_track_list.json')
        ids = mxo.get_mxids(mx_objects)
        self.assertEquals(92, len(ids))

    def test_valid_color_set(self):
        color_sets = futil.from_file('tests/mx_sample_cs.json')
        valid_color_sets = mxo.valid_color_sets(color_sets)
        self.assertEquals(1, len(valid_color_sets))


    def test_invalid_color_set(self):
        color_sets = futil.from_file('tests/mx_sample_cs_incomplete.json')
        valid_color_sets = mxo.valid_color_sets(color_sets)
        self.assertEquals(0, len(valid_color_sets))

    def struct_time(self):
        ''' Test converting mx GMT time string into Python struct_time. '''
        cs = futil.from_file('tests/mx_sample_cs_compound.json')
        results = [mxo.creationdate(mx_colorset) for mx_colorset in cs]
        self.assertEquals([(2013, 8, 20, 23, 27, 34, 1, 232, 0),
                           (2013, 8, 20, 20, 38, 25, 1, 232, 0),
                           (2013, 8, 19, 10, 55, 59, 0, 231, 0)], results)

    def test_get_creationdate(self):
        ''' Find a colorset's creation date. '''
        cs = futil.from_file('tests/mx_sample_cs_compound.json')
        results = [mxo.creationdate(mx_colorset) for mx_colorset in cs]
        self.assertEquals(['2013-08-20 23:27:34 GMT', '2013-08-20 20:38:25 GMT',
            '2013-08-19 10:55:59 GMT'], results)

    def test_get_passfail(self):
        cs = futil.from_file('tests/mx_sample_cs_compound.json')
        results = [mxo.passfail(mx_colorset) for mx_colorset in cs]
        self.assertEquals(['P', 'P', 'P'], results)

    def test_get_rows(self):
        cs = futil.from_file('tests/mx_sample_cs_compound.json')
        cscl = mxo.get_cscl(cs[0], '232301')
        fields, rows = mxo.get_rows(cscl)
        self.assertEquals(10, len(fields))
        self.assertEquals(27, len(rows))

class TestMxCore(unittest.TestCase):
    def setUp(self):
        shutil.rmtree(TEMP_CACHE_PATH + '/0/2', ignore_errors = True)
        os.makedirs(TEMP_CACHE_PATH + '/0/2')
        shutil.copy(TEST_CACHE_PATH + '/0/2/32', TEMP_CACHE_PATH + '/0/2')

    def tearDown(self):
        shutil.rmtree(TEMP_CACHE_PATH + '/0/2', ignore_errors = True)
        shutil.rmtree(NONEXISTENT_CACHE_PATH, ignore_errors = True)

    def test_get_cache_file_path(self):
        ''' Calculate cache file paths for 5, 6, 7 character length mxids. '''
        test1 = 10000
        test1out = 'cscl_cache/0/0/10'
        test2 = 100001
        test2out = 'cscl_cache/0/1/0'
        test3 = 255000
        test3out = 'cscl_cache/0/2/55'
        test4 = 1000000
        test4out = 'cscl_cache/1/0/0'
        test5 = 4999999
        test5out = 'cscl_cache/4/9/99'
        self.assertEquals(test1out, cache._get_cache_file_path('cscl_cache', test1))
        self.assertEquals(test2out, cache._get_cache_file_path('cscl_cache', test2))
        self.assertEquals(test3out, cache._get_cache_file_path('cscl_cache', test3))
        self.assertEquals(test4out, cache._get_cache_file_path('cscl_cache', test4))
        self.assertEquals(test5out, cache._get_cache_file_path('cscl_cache', test5))

    def test_cache_files_for_mxids(self):
        cache_filenames = cache._get_cache_filenames_for_mxids('cscl_cache', ['229999', '229994', '230662', '230600'])
        self.assertItemsEqual(['229999', '229994'], cache_filenames['cscl_cache/0/2/29'])
        self.assertItemsEqual(['230662', '230600'], cache_filenames['cscl_cache/0/2/30'])

    def test_save_mx_objects_to_nonexistent_cache(self):
        ''' Save to a cache directory that hasn't been created yet. '''
        self.assertFalse(os.path.isdir(NONEXISTENT_CACHE_PATH))
        # Get a valid JSON string
        json_string = open('tests/mx_sample_cs_compound.json').read()
        saved_mxids = cache.save_mx_objects(NONEXISTENT_CACHE_PATH, json.loads(json_string))
        self.assertTrue(os.path.isdir(NONEXISTENT_CACHE_PATH))
        self.assertEquals(['232301', '232245', '231407'], saved_mxids)

    def test_save_big_mx_object_set_to_cache(self):
        ''' Save mxobjects to a cache directory. '''
        # Get a valid JSON string
        json_string = open('tests/mx_sample_cs_c_50.json').read()
        saved_mxids = cache.save_mx_objects(TEMP_CACHE_PATH, json.loads(json_string))
        self.assertEquals(50, len(saved_mxids))

    def test_save_mx_objects_to_cache(self):
        ''' Save mxobjects to a cache directory. '''
        # Get a valid JSON string
        json_string = open('tests/mx_sample_cs_compound.json').read()
        saved_mxids = cache.save_mx_objects(TEMP_CACHE_PATH, json.loads(json_string))
        self.assertEquals(['231407'], saved_mxids)

    def test_load_mx_objects_from_cache(self):
        mx_objects = cache.load_mx_objects(TEMP_CACHE_PATH, ['232301', '232302'])
        self.assertTrue('232301' in mx_objects.keys())
        self.assertFalse('232302' in mx_objects.keys())
        mxids = mxo.get_mxids(mx_objects['232301'])
        self.assertEquals(['232301'], mxids)

if __name__ == '__main__':
    unittest.main()
