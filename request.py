import copy
import util.httputil as httputil
import json
import time
import urllib
import urllib2

URL = 'https://www2.chromix.com/maxwell/api/rest/v1/?'
CREDENTIALS = {'username': 'fmahnke', 'password': 'fm@hnk3'}

p = {'signin': 'signin',
    'session': '-session',
    'objects': 'APIMxOL:0'}

class Request:
    ''' Makes requests to Maxwell using the MxAPI. '''

    def __init__(self, retry_delay, logger):
        self.log = logger
        self.last_request_time = 0
        self.session_id = self._get_new_session_id()
        self.retry_delay = retry_delay

    def _get(self, params):
        self._request_wait()
        params[p['session']] = self.session_id
        url = httputil.build_url(URL, params)
        try:
            self.log.debug('GET ' + url)
            response = httputil.get(url)
        except urllib2.HTTPError, e:
            if self._is_urlerror_retriable(e):
                response = self._get(params)

        return response

    def _request_wait(self):
        if self.last_request_time > 0:
            self.log.debug('Waiting ' + str(self.retry_delay) + ' before next request')
            while (time.time() - self.last_request_time < self.retry_delay):
                pass

        self.last_request_time = time.time()

    def _get_new_session_id(self):
        self._request_wait()
        try:
            response_data = httputil.post(URL + 'r=' + p['signin'], CREDENTIALS)
            self.log.info(response_data)
        except urllib2.URLError, e:
            self.log.error(e)

        resp = json.loads(response_data)
        return 'sessID:' + resp['session']['id']

    def _is_urlerror_retriable(self, e):
        self.log.error(e.code)
        error = e.read()
        self.log.error(error)
        if e.code == 403:
            self.session_id = self._get_new_session_id()
            return True
        if e.code == 500:
            return True

    def _get_by_mxid(self, request_param, mxids):
        ''' Get a list of MXobjects by MxID. '''
        return_json = '['
        results = []
        for mxid in mxids:
            mxid_index = mxids.index(mxid)

            self.log.info('Requesting %s (%s of %s).', mxid, mxid_index + 1, len(mxids))

            this_request_param = copy.deepcopy(request_param)
            this_request_param['r'] += mxid
            results.append(self._get(this_request_param))
        for result in results:
            return_json += result + ','

        return_json = return_json[:-1] + ']'
        return json.loads(return_json)

    def user_contact_list(self, user):
        ''' Get the list of contacts associated with a user (UcontactL). '''
        return json.loads(self._get({'r': 'UcontactL:' + user}))

    def user_device_list(self, user):
        ''' Get the list of devices associated with a user (UDL). '''
        return json.loads(self._get({'r': 'UDL:' + user}))

    def device_track_list(self, device_mxid):
        ''' Get the list of tracks associated with a device (DTL). '''
        self.log.info("Fetching track list for mxid " + device_mxid)
        return json.loads(self._get({'r': 'DTL:' + device_mxid}))

    def user_track_list(self, user):
        ''' Get the list of tracks associated with a user (UTL). '''
        return self._get({'r': 'UTL:' + user})

    def track_color_set_list(self, track_mxid):
        ''' Get the list of Color Sets (list of measurements) associated with a Track. '''
        self.log.info("Fetching track color set list for mxid " + str(track_mxid))
        return json.loads(self._get({'r' : 'TCSL:' + str(track_mxid)}))

    def color_set_compound(self, mxids):
        ''' Get the compound form of a color set (CS with CSCL). '''
        request_param = {'r': 'CS:', 'objectPortion': 'compound'}
        return self._get_by_mxid(request_param, mxids)

