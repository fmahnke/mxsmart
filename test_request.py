import json
import mox
import unittest

import util.fileutil as futil
import request

class TestRequest(unittest.TestCase):
    def setUp(self):
        self.mox = mox.Mox()
        self.req = self.mox.CreateMock(request.Request)

    def test_user_device_list(self):
        dev_list = futil.from_file('tests/mx_sample_dev_list.json')
        self.req._get({'r': 'UDL:30238'}).AndReturn(dev_list)
        self.mox.ReplayAll()

        result = self.req._get({'r': 'UDL:30238'})
        self.assertItemsEqual(dev_list, result)
        self.mox.VerifyAll()

    def test_device_track_list(self):
        track_list = futil.from_file('tests/mx_sample_dev_track_list.json')
        self.req._get({'r': 'DTL:11111'}).AndReturn(track_list)
        self.mox.ReplayAll()

        result = self.req._get({'r': 'DTL:11111'})
        self.assertItemsEqual(track_list, result)
        self.mox.VerifyAll()

    def test_track_color_set_list(self):
        csl = futil.from_file('tests/mx_sample_tcsl.json')
        self.req._get({'r': 'TCSL:11111'}).AndReturn(csl)
        self.mox.ReplayAll()

        result = self.req._get({'r': 'TCSL:11111'})
        self.assertItemsEqual(csl, result)
        self.mox.VerifyAll()

    '''
    def test_sign_in(self):
        # todo
        success = '{"session": {"id": "62A54B6C1632136AA6lpK1103CBC", "userID": "30238", "ttl": 240, "username": "fmahnke"}, "MXapi": {"version": "1.0b10", "lastErrorCode": "200", "lastErrorMsg": "user access granted"}}'
        self.assertIsNotNone(None)

    def test_access_failed(self):
        # todo
        failed = '{"session": {"id": "4468C48A160013422FIwY2B14C61", "userID": "", "ttl": 240, "username": "undefined"}, "MXapi": {"version": "1.0b10", "lastErrorCode": "403", "lastErrorMsg": "user access failed"}}'
        self.assertIsNotNone(None)
    '''

    def tearDown(self):
        self.mox.UnsetStubs()

if __name__ == '__main__':
        unittest.main()

