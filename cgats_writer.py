def header(creation_date):
    return 'MxDATE\t' + creation_date + '\n'

def data_format(fields):
    return 'BEGIN_DATA_FORMAT\n' + fields + '\nEND_DATA_FORMAT\n'

def data(data):
    return 'BEGIN_DATA\n' + data + 'END_DATA\n'
