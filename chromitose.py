from flask import Flask
from flask import request
from flask import Response
from flask import jsonify
from flask import render_template
import time
import json
import mx_core as mx_c

app = Flask(__name__)
app.debug = True
mx = mx_c.Maxwell()

@app.route('/')
def index():
    return "<span style='color:red'>I am app 2</span>"

@app.route('/health')
def health():
    return "Up and running", 200

@app.route('/get_colorset')
def get_colorset():
    options = json.loads(request.args.get('options'))
    print options
    dev = options['device']
    if 'track' in options:
        track = options['track']
    else:
        track = None

    if 'filter' in options:
        start_time = time.strptime(options['filter']['startDateTime'], '%Y-%m-%d %H:%M:%S %Z')
        end_time = time.strptime(options['filter']['endDateTime'], '%Y-%m-%d %H:%M:%S %Z')
    else:
        start_time = None
        end_time = None

    zip_file = mx.get_colorsets(dev, track_name=track, start_time=start_time, end_time=end_time)

    if zip_file is not None:
        # return jsonify({'results': cs})
        with open(zip_file) as f:
            resp = f.read()
        return Response(resp, mimetype = 'application/zip')
    else:
        return render_template('get_colorset_none.html',
                message='Request successful. No colorsets were found for the request criteria.'), 204

@app.route('/get_passfail')
def get_passfail():
    options = json.loads(request.args.get('options'))
    print options
    dev = options['device']
    start_time = time.strptime(options['filter']['startDateTime'], '%Y-%m-%d %H:%M:%S %Z')
    end_time = time.strptime(options['filter']['endDateTime'], '%Y-%m-%d %H:%M:%S %Z')

    results = mx.get_api_passfail(dev, start_time = start_time, end_time = end_time)
    return jsonify({'results': results})

@app.route('/update_cache')
def update_cache():
    result = mx.update_cache()

    if result == True:
        return '', 200
    else:
        return '', 500
