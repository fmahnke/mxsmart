import util.fileutil as futil
import json
import mx_object as mxo

import zc.lockfile

def _get_cache_file_path(cache_path, num):
    million = num / 1000000
    num -= million * 1000000
    hundred_thousand = num / 100000
    num -= hundred_thousand * 100000
    num /= 1000
    return cache_path + '/' + str(million) + '/' + str(hundred_thousand) + '/' + str(num)

def _get_cache_filenames_for_mxids(cache_path, mxids):
    '''
    Given a list of mxids, return a dict containing unique cache filenames and a
    list of corresponding mxids.
    '''

    cache_files = {}
    for mxid in mxids:
        filename = _get_cache_file_path(cache_path, int(mxid))
        if not filename in cache_files:
            cache_files[filename] = [mxid]
        else:
            cache_files[filename].append(mxid)
    return cache_files

def _load_from_cache(cache_data, mxid):
    if cache_data:
        for d in cache_data:
            if d['MXobject']['MxID'].split(':')[-1] == mxid:
                # Have it in the cache already
                return d

def _write_to_cache(cache_file_path, cache_data, mx_object, mxid):
    cache_data.append(mx_object)
    with open(cache_file_path, 'w') as f:
        f.write(json.dumps(cache_data))
    return mxid

def save_mx_objects(cache_path, mx_objects):
    assert cache_path is not None

    ''' Save MX objects to the local cache. '''
    mxids = []
    for obj in mx_objects:
        mxo_ids = mxo.get_mxids(obj)
        assert len(mxo_ids) == 1
        mxids.append(mxo_ids[0])
    saved_mxids = []
    assert len(mx_objects) == len(mxids)
    for i in range(len(mxids)):
        # Find the right place in the cache
        cache_file_path = _get_cache_file_path(cache_path, int(mxids[i]))

        futil.make_containing_directory_from_file_path(cache_file_path)

        cache_data = futil.from_file(cache_file_path)

        if cache_data:
            if not _load_from_cache(cache_data, mxids[i]):
                saved_mxids.append(_write_to_cache(cache_file_path, cache_data, mx_objects[i], mxids[i]))
        else:
            # Cache file doesn't exist. Create it
            data = []
            saved_mxids.append(_write_to_cache(cache_file_path, data, mx_objects[i], mxids[i]))

    return saved_mxids

def load_mx_objects(cache_path, mxids):
    ''' Load MX objects from the local cache. '''
    mx_objects = {}
    cache_file_paths = _get_cache_filenames_for_mxids(cache_path, mxids)

    for filename, file_mxids in cache_file_paths.iteritems():
        cache_data = futil.from_file(filename)
        for file_mxid in file_mxids:
            mxo = _load_from_cache(cache_data, file_mxid)

            if mxo:
                mx_objects[file_mxid] = mxo

    if mx_objects:
        return mx_objects

def get_lock():
    ''' Attempt to get a lock on the cache. '''
    try:
        lock = zc.lockfile.LockFile('cache_lock')
    except zc.lockfile.LockError:
        lock = None

    return lock

def unlock(lock):
    ''' Release the lock on the cache. '''
    lock.close()
